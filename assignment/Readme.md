# Readme

## what has been achieved ?
- Infrastructure as Code
  - AWS VPC creation
  - AWS EKS cluster creation
- K8s Application
  - Installed Helm tiller in the k8s cluster
  - Installed K8s Dashboard in k8s cluster
  - Creation of Java REST API with the help of Spring Boot
  - Simple Docker file creation
  - Deployment of application as a Helm chart.
  - Pull config map and secret in Java application. Please refer MongoConfig and Deployment.yaml
  - Used MongoDB as sub chart.it provides a facility to run mongo in replication( i.e.primary and secondary. secondary useful for a read operation or taking a backup)


## Structure of this Assignment
This assignment on high level can be divided in two parts i.e. terraform scripts and java application with helm chart

- __infrastruture-as-code__ - Place for terraform scripts
  - tf-modules - 
    - imp notes
	  - Contains terraform modules used for this assignment
	  - For more information refer readme files present at various locations.
      - This work is based on my existing  tf modules. [Refer]( https://github.com/polganesh/terraform-modules)
	- structure
	  - /cloud/aws/networking/vpc - vpc modules
	  - /cloud/aws/computing/orchestration/eks - AWS EKS module
  - container-soltn-tf - using terraform module defined in tf-modules
    - structure
	  - /assignment/non-prod/base/vpc 
		- imp notes
		  - tf script for creating VPC
          - This is base module and must be first step for bootstrapping this assignment
      - /assignment/non-prod/dev
	    - imp notes
		  - tf script for creating EKS
		  - this script has dependency on vpc script   	  
- __java-spring-boot-app__ 
  - imp notes
    - Place for Java application code
    - Please refer read me file present inside this project for building and deploying it to k8s cluster

## Ideal AWS VPC and EKS cluster
[Refer]( https://github.com/polganesh/wiki-images/blob/master/terraform-examples/ideal-aws-vpc-cluster.png)

## Limitations
- in current assignment we are using ELB (classic) load balancer. ideally it should use Nginx or ALB i.e. type 7 Load balancer
- Enable security group for EKS cluster in a more better way.
- Mongo back up with the help of tools like MGOB	
- Auto scaling for EKS is not tested
- Need to check if AWS EKS support serverless feature i.e. [Virtual Kubelet]( https://azure.microsoft.com/en-us/resources/videos/azure-friday-virtual-kubelet-introduction/)
- Current java project using simple mongo db driver. we can try reactive driver for mongodb.
